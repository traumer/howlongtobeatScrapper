# This Python file uses the following encoding: utf-8
from lxml import html
import requests
import sys
import unicodedata
from operator import itemgetter

def rawScrap(gamename):
    games=daScrappin(gamename,True)
    print html.tostring(games)

def daScrappin(gamename,showHtml):
    url = 'https://howlongtobeat.com/search_main.php?page=1'
    params = {'t': 'games', 'page': '1', 'sorthead': 'popular', 'sortd': 'Normal Order', 'plat': '', 'detail': '0'}
    gamepost = {'queryString': gamename}
    r = requests.post(url,data=gamepost)
    tree = html.fromstring(r.content)
    if showHtml==False:
        return tree.xpath('//li[@class="back_white shadow_box"]')
    else:
         return tree

def gameLenghtDict(gamename):
    games=daScrappin(gamename,False)
    if len(games)<1:
        title=gamename
        main=None
        complete=None
    else:
        value=games[0]
        superdiv=value.find("div[@class='search_list_details']")
        title=superdiv.find("h3/a").get('title')
        main=superdiv.findall("div/div")[0].findall("div")[1].text
        main=main.replace(u"½", u".5")
        main=main.replace(u"--","-1")
        main=main.replace("Hours","")
        main=float(main.strip())
        complete=superdiv.findall("div/div")[2].findall("div")[1].text
        complete=complete.replace(u"½", u".5")
        complete=complete.replace("Hours","")
        complete=complete.replace(u"--","-1")
        complete=float(complete.strip())
    return {'title':title,'main':main,'complete':complete}

if __name__=="__main__":
        gameList=open("gameList","r")
        gameFormated=gameList.read().splitlines()
        gameMap=[]
        for game in gameFormated:
            if len(sys.argv)>1:
                print("-----"+game+"--------")
                print rawScrap(game)
            else:
                gameMap.append(gameLenghtDict(game))
                print("read "+str(len(gameMap))+" games out of "+str(len(gameFormated)))
                gameMap=sorted(gameMap,key=lambda game:game['main'])
        for game in gameMap:
            print(game['title']+" : "+str(game['main'])+" / "+str(game['complete']))
